package com.pattern.creational.builder;

public interface Packing {
    public String pack();
}
