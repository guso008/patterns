package com.pattern.creational.abstractfactory;

public interface Shape {
    void draw();
}
